import React from "react"
import { render } from "@testing-library/react"
import CloseIcon from "./CloseIcon"

it("should match snapshot with default values", () => {
  const { container } = render(<CloseIcon />)
  expect(container).toMatchInlineSnapshot(`
    <div>
      <button
        class="sc-bdfBwQ krgnyt"
      >
        <svg
          aria-label="Close"
          enable-background="new 0 0 16 16"
          id="Layer_1"
          version="1.1"
          viewBox="0 0 17 17"
          x="0px"
          y="0px"
        >
          <title>
            Close Icon
          </title>
          <path
            d="M9.2,8.5l7.1-7.1c0.2-0.2,0.2-0.5,0-0.7s-0.5-0.2-0.7,
              0L8.5,7.8L1.4,0.6c-0.2-0.2-0.5-0.2-0.7,0s-0.2,0.5,0,
              0.7l7.1,7.1l-7.1,7.1c-0.2,0.2-0.2,0.5,0,0.7c0.1,0.1,
              0.2,0.1,0.4,0.1s0.3,0,0.4-0.1l7.1-7.1l7.1,7.1c0.1,
              0.1,0.2,0.1,0.4,0.1s0.3,0,0.4-0.1c0.2-0.2,0.2-0.5,
              0-0.7L9.2,8.5z"
          />
        </svg>
      </button>
    </div>
  `)
})
