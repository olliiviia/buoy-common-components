import React from "react"
import { Story, Meta } from "@storybook/react"
import styled from "styled-components"
import theme from "../buoy-theme"
import CloseIcon, { ICloseIconProps } from "./CloseIcon"

const Container = styled.div`
  width: 300px;
  height: 200px;
  border: 1px solid black;
  position: relative;
  margin: 20px auto 0;
`

export default {
  title: "CloseIcon",
  component: CloseIcon,
  argTypes: {
    color: {
      control: {
        type: "select",
        options: theme.colors,
      },
    },
    size: { control: "number" },
    padding: { control: "number" },
    backgroundSize: { control: "number" },
    iconSize: { control: "number" },
    right: { control: "number" },
    hoverColor: {
      control: {
        type: "select",
        options: Object.keys(theme.colors),
      },
    },
    top: { control: "number" },
    topBanner: { control: "boolean" },
  },
} as Meta

const Template: Story<ICloseIconProps> = ({
  color,
  size,
  padding,
  backgroundSize,
  iconSize,
  right,
  top,
  ...rest
}) => (
  <Container>
    <CloseIcon
      color={color}
      size={size ? `${size}px` : undefined}
      padding={padding ? `${padding}px` : undefined}
      backgroundSize={backgroundSize ? `${backgroundSize}px` : undefined}
      iconSize={iconSize ? `${iconSize}px` : undefined}
      right={right ? `${right}px` : undefined}
      top={top ? `${top}px` : undefined}
      {...rest}
    />
  </Container>
)

export const Default = Template.bind({})
