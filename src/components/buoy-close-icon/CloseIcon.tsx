import React from "react"
import styled, { css } from "styled-components"

export interface ICloseIconProps {
  /** Color of the icon */
  color?: string
  /** Size of the button; e.g. "30px" */
  size?: string
  /** CSS `right` attribute on the button; e.g. "10px" */
  right?: string
  /** CSS `padding` attribute on the button; e.g. "5px" */
  padding?: string
  /** Size of the icon */
  iconSize?: string
  /** Color of background on hover */
  hoverColor?: string
  /** Size of the background */
  backgroundSize?: string
  /** CSS `top` attribute on the button */
  top?: string
  /** Whether banner should have a high z-index */
  topBanner?: boolean
  /** App theme */
  theme?: any
  /** Callback when button is clicked */
  onClick?: () => void
}

const CloseIconWrapper = styled.button`
  display: block;
  width: ${(props: ICloseIconProps) => props.size || "35px"};
  height: ${(props: ICloseIconProps) => props.size || "35px"};
  cursor: pointer;
  position: absolute;
  top: ${(props: ICloseIconProps) => (props.top ? props.top : "0px")};
  right: ${(props: ICloseIconProps) => (props.right ? props.right : "0px")};
  z-index: ${(props: ICloseIconProps) => (props.topBanner ? 999 : 1)};
  background: none;
  border: none;
  padding: ${(props: ICloseIconProps) =>
    props.padding ? props.padding : "none"};
  margin: 0;

  svg {
    position: absolute;
    top: ${(props: ICloseIconProps) =>
      props.padding ? props.padding : "10px"};
    left: ${(props: ICloseIconProps) =>
      props.padding ? props.padding : "10px"};
    width: ${(props: ICloseIconProps) =>
      props.iconSize ? props.iconSize : "15px"};
    height: ${(props: ICloseIconProps) =>
      props.iconSize ? props.iconSize : "15px"};

    path {
      fill: ${(props: ICloseIconProps) =>
        props.color ? props.color : "black"};
    }
  }

  @media (hover: hover) {
    &:hover:before {
      content: "";
      background: ${(props: ICloseIconProps) =>
        props.hoverColor
          ? props.theme.colors[props.hoverColor]
          : "rgba(0, 173, 239, 0.1)"};
      ${(props: ICloseIconProps) =>
        props.hoverColor &&
        css`
          opacity: 0.1;
        `};
      height: ${(props: ICloseIconProps) =>
        props.backgroundSize ? props.backgroundSize : "37.5px"};
      width: ${(props: ICloseIconProps) =>
        props.backgroundSize ? props.backgroundSize : "37.5px"};
      border-radius: 50%;
      position: absolute;
      left: 50%;
      top: 50%;
      transform: translateX(-50%) translateY(-50%);
      z-index: -1;
    }
  }
  :focus {
    outline: 3px solid #fff;
    background-color: #0042cb;
    box-shadow: inset 0 0 20px 0 rgba(0, 0, 0, 0.3);
  }
`

const CloseIcon = (props: ICloseIconProps) => (
  <CloseIconWrapper {...props}>
    <svg
      version="1.1"
      id="Layer_1"
      x="0px"
      y="0px"
      viewBox="0 0 17 17"
      enableBackground="new 0 0 16 16"
      aria-label="Close"
    >
      <title>Close Icon</title>
      <path
        d="M9.2,8.5l7.1-7.1c0.2-0.2,0.2-0.5,0-0.7s-0.5-0.2-0.7,
          0L8.5,7.8L1.4,0.6c-0.2-0.2-0.5-0.2-0.7,0s-0.2,0.5,0,
          0.7l7.1,7.1l-7.1,7.1c-0.2,0.2-0.2,0.5,0,0.7c0.1,0.1,
          0.2,0.1,0.4,0.1s0.3,0,0.4-0.1l7.1-7.1l7.1,7.1c0.1,
          0.1,0.2,0.1,0.4,0.1s0.3,0,0.4-0.1c0.2-0.2,0.2-0.5,
          0-0.7L9.2,8.5z"
      />
    </svg>
  </CloseIconWrapper>
)

export default CloseIcon
