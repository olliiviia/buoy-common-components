const fonts = {
  family: '"Roboto", Arial, Helvetica, sans-serif',
  weight: {
    light: 400,
    regular: 500,
    medium: 700,
  },
  size: {
    h1: {
      xl: "40px",
      lg: "40px",
      md: "34px",
      sm: "34px",
      xs: "34px",
    },
    h2: {
      xl: "32px",
      lg: "32px",
      md: "30px",
      sm: "30px",
      xs: "30px",
    },
    h3: {
      xl: "26px",
      lg: "26px",
      md: "24px",
      sm: "24px",
      xs: "24px",
    },
    label: {
      xl: "11px",
      lg: "11px",
      md: "11px",
      sm: "11px",
      xs: "11px",
    },
    bodySmall: {
      xl: "12px",
      lg: "12px",
      md: "12px",
      sm: "12px",
      xs: "12px",
    },
    bodyMain: {
      xl: "16px",
      lg: "16px",
      md: "16px",
      sm: "16px",
      xs: "16px",
    },
    bodyLarge: {
      xl: "18px",
      lg: "18px",
      md: "18px",
      sm: "18px",
      xs: "18px",
    },
    bodyLarger: {
      xl: "20px",
      lg: "20px",
      md: "20px",
      sm: "20px",
      xs: "20px",
    },
  },
}

export { fonts as default }
