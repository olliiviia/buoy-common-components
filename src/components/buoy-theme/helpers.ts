import theme, { ITheme } from "./theme"
import responsive from "./responsive"
import { fonts } from "."

interface IThemeProps {
  theme: ITheme
}

const createGetter = (dict: object, base: (props: IThemeProps) => any) => {
  const getter: { [key: string]: any } = {}
  Object.keys(dict).forEach(
    (item) => (getter[item] = (props: IThemeProps) => base(props)[item])
  )
  return getter
}

export const colors = createGetter(
  theme.colors,
  (props: IThemeProps) => props.theme.colors
)

export const constants = createGetter(
  theme.constants,
  (props: IThemeProps) => props.theme.constants
)

export const fontFamily = (props: IThemeProps) => props.theme.fonts.family

export const fontWeight = createGetter(
  theme.fonts.weight,
  (props: IThemeProps) => props.theme.fonts.weight
)

export const fontSize = (size: keyof typeof fonts.size) => (
  props: IThemeProps
) => {
  return responsive(
    "font-size",
    props.theme.fonts.size[size].xl,
    props.theme.fonts.size[size].lg,
    props.theme.fonts.size[size].md,
    props.theme.fonts.size[size].sm,
    props.theme.fonts.size[size].xs
  )
}
