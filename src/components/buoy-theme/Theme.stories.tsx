import React from "react"
import { Story, Meta } from "@storybook/react"
import styled from "styled-components"
import { fontSize, fontFamily } from "./index"
import fonts from "./fonts"
import colors from "./colors"

const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 12px;
  width: 100%;
`

const Paragraph = styled("p")<{
  color: keyof typeof colors
  contrast: boolean
  size?: keyof typeof fonts.size
}>`
  display: flex;
  align-items: center;
  font-family: ${fontFamily};
  background-color: ${(props) => colors[props.color]};
  color: ${(props) => (props.contrast ? "black" : "white")};
  text-align: center;
  padding: 20px 20px;
  width: 100%;
  height: 100%;
  border-radius: 2px;
  margin-bottom: 10px;
  ${(props) => (props.size ? fontSize(props.size) : fontSize("bodyMain"))};
`

const Table = styled.table`
  border-collapse: collapse;
`

const TableRow = styled["tr"]<{ size: keyof typeof fonts.size }>`
  font-family: ${fontFamily};
  ${(props) => (props.size ? fontSize(props.size) : fontSize("bodyMain"))};
`

const TableCell = styled.td`
  padding: 12px;
  border: solid 1px ${colors.neutral};
`

const contrast: Partial<keyof typeof colors>[] = [
  "primaryLightest",
  "fantasy",
  "white",
  "landingBackground",
  "secondaryLightest",
  "tertiaryLightest",
  "neutralLightest",
  "landingYellowLight",
  "landingRedLight",
  "transparent",
]

export default {
  title: "Theme",
} as Meta

const ColorTemplate: Story = () => (
  <Grid>
    {Object.keys(colors).map((key) => (
      <Paragraph
        color={key as keyof typeof colors}
        contrast={contrast.includes(key as keyof typeof colors)}
        key={key}
      >
        {key} {colors[key as keyof typeof colors]}
      </Paragraph>
    ))}
  </Grid>
)

export const Colors = ColorTemplate.bind({})

const FontSizeTemplate: Story = () => (
  <Table>
    {Object.keys(fonts.size).map((key) => (
      <TableRow size={key as keyof typeof fonts.size} key={key}>
        <TableCell style={{ color: "#0042cb" }}>{key}</TableCell>
        {Object.keys(fonts.size[key as keyof typeof fonts.size]).map(
          (breakpoint) => (
            <TableCell key={key + breakpoint}>
              {breakpoint}:{" "}
              {
                fonts.size[key as keyof typeof fonts.size][
                  breakpoint as keyof typeof fonts.size.h1
                ]
              }{" "}
            </TableCell>
          )
        )}
      </TableRow>
    ))}
  </Table>
)

export const FontSizes = FontSizeTemplate.bind({})
