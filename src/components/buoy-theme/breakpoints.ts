const breakpoints = {
  xs: "479",
  sm: "768",
  md: "1019",
  lg: "1319",
}

export { breakpoints as default }
