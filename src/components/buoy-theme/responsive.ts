import { css } from "styled-components"
import breakpoints from "./breakpoints"

const responsive = (field, xl, lg, md, sm, xs) => css`
  ${field}: ${xl};
  ${xl !== lg &&
  `
  @media(max-width: ${breakpoints.lg + "px"}) {
    ${field}: ${lg};
  }
  `}
  ${lg !== md &&
  `
  @media(max-width: ${breakpoints.md + "px"}) {
    ${field}: ${md};
  }
  `}
  ${md !== sm &&
  `
  @media(max-width: ${breakpoints.sm + "px"}) {
    ${field}: ${sm};
  }
  `}
  ${sm !== xs &&
  `
  @media(max-width: ${breakpoints.xs + "px"}) {
    ${field}: ${xs};
  }
  `}
`

export { responsive as default }
