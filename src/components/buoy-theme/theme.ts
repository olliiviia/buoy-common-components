import colors from "./colors"
import constants from "./constants"
import fonts from "./fonts"

const theme = { colors, constants, fonts }

export type ITheme = typeof theme

export { theme as default }
