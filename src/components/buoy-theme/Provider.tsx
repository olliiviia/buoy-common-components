import React from "react"
import { ThemeProvider } from "styled-components"
import theme from "./theme"

const Provider = ({ children }: { children: React.ReactNode }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
)

export { Provider as default }
