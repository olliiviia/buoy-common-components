const constants = {
  borderRadius: "4px",
  borderWidth: "1px",
  boxShadow: "0 0 12px 0 rgba(0,0,0,0.1)",
  boxShadowHover: "0 0 16px 0 rgba(0,0,0,0.2)",
}

export { constants as default }
