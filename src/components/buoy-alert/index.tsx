import React from "react"
import styled from "styled-components"
import CloseIcon from "../buoy-close-icon"
import { colors, fontFamily } from "../buoy-theme"

const Message = styled.div`
  display: inherit;
  width: 100%;
  height: auto;
  padding: 13px 4px 12px 4px;
  justify-content: center;
  flex-basis: 90%;
  text-decoration: none;
`

const MessageWrapper = styled.div`
  display: inherit;
  text-align: center;
  justify-content: center;
  flex-wrap: wrap;
  width: 97.5%;
  white-space: pre-wrap;
`

const MessageText = styled.span`
  font-family: ${fontFamily};
  color: ${colors.neutralLightest};
  font-size: 12px;
  line-height: 1.25;
  letter-spacing: 0.1px;
`

const Alert = styled.div`
  width: 100%;
  @media (max-width: 768px) {
    width: 90%;
  }
  z-index: 98;
  opacity: 0.9;
`

interface IAlertWrapperProps {
  visible: boolean
  topBanner: boolean
}

const AlertWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  transform: ${(props: IAlertWrapperProps) =>
    props.visible ? "translateY(0)" : "translateY(-250%)"};
  transition: transform 0.3s ease;
  ${(props: IAlertWrapperProps) => (props.topBanner ? "top: 0" : "top: 1px")};
  background-color: #196cff;
  :hover {
    background-color: #033db6;
    transition: 0.3s ease;
  }
`

interface IAlertMessageProps {
  message: { text: string[] }
}

const AlertMessage = (props: IAlertMessageProps) => {
  const { message } = props
  return (
    <Message>
      <MessageWrapper>
        {message?.text.map((msg) => (
          <MessageText key={msg}>{msg}</MessageText>
        ))}
      </MessageWrapper>
    </Message>
  )
}

interface IAlertBannerProps {
  variant: string
  topBanner: boolean
  onDismiss: () => void
}

interface IAlertBannerState {
  visible: boolean
  aboveNav: boolean
  height: number
}

class AlertBanner extends React.Component<
  IAlertBannerProps,
  IAlertBannerState
> {
  constructor(props: IAlertBannerProps) {
    super(props)
    this.state = { visible: false, aboveNav: true, height: 0 }
    this.alertRef = React.createRef()
  }

  private alertRef: any

  componentDidMount() {
    // Do this to show initial transition in
    this.setState({
      visible: true,
      height: this.alertRef.current.clientHeight,
    })
    window.addEventListener("resize", this.handleResize)
  }

  // detect alert height change if user resizes browser
  handleResize = () => {
    if (
      this.alertRef &&
      this.alertRef.current &&
      this.alertRef.current.clientHeight !== this.state.height
    )
      this.setState({ height: this.alertRef.current.clientHeight })
  }

  dismiss = () => {
    this.setState({ visible: false, aboveNav: false }, () => {
      setTimeout(() => {
        if (this.props.onDismiss) {
          this.props.onDismiss()
        }
      }, 500)
    })
  }

  getMessage = () => {
    // Default message if no variant given
    return {
      type: "default",
      text: ["This is an alert banner with placeholder text."],
    }
  }

  render() {
    const { topBanner } = this.props
    const { visible } = this.state

    return (
      <AlertWrapper visible={visible} topBanner={topBanner} ref={this.alertRef}>
        <Alert>
          <AlertMessage message={this.getMessage()} />
        </Alert>

        <CloseIcon
          size="36px"
          iconSize="16px"
          color={colors.neutralLightest}
          topBanner={topBanner}
          onClick={this.dismiss}
          padding="10px"
          top="6px"
          right="12px"
        />
      </AlertWrapper>
    )
  }
}

export { AlertBanner as default }
