# Common Components w/Bit

## Storybook

`npm run storybook` builds and runs storybook at [http://localhost:6006](http://localhost:6006).

## Running tests

`npm test` launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Bit Workflow

We use [Bit](https://docs.bit.dev/docs/quick-start) to version, host, and share our components.

Opening a PR in this repo will automatically build & test any components you've edited.

Once the PR is merged, a pipeline will tag new versions and export any changes to [bit.dev](https://bit.dev).

To add a new component:

1. Add a new directory in src/components with the naming convention `buoy-my-new-component`.
2. In this directory add: (\*suggested files, please change as needed)
   - `index.ts`
   - `MyNewComponent.tsx`
   - `MyNewComponent.spec.tsx`
   - `MyNewComponent.stories.tsx`
3. Write your new component, along with accompanying stories and tests.
4. In `index.ts`, export all modules you want to be available with this component.
5. To start tracking this component with bit, run `bit add src/components/my-new-component`
6. To track your tests, run `bit add src/components/my-new-component --tests src/components/my-new-component/MyNewComponent.spec.tsx`
7. You should see local changes in `.bitmap`. Commit and push these changes with your new component.
8. You can check that your component is being tracked by running `bit status`.

### Known Issues w/Testing + Bit

1. Snapshot testing is not supported in the CI (snapshots are not discovered). You can mostly get around this by using `toMatchInlineSnapshot` instead, although this isn't foolproof because of issue #2.
2. The available bit tester uses a very old version of Jest (22.4.3 - vs latest = 26.6.3). This repo uses 26.6.3. Therefore, it's possible that running `npm test` passes, but `bit test` fails.
3.
