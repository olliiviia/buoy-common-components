import React from "react"
import { createGlobalStyle } from "styled-components"
import { Provider } from "../src/components/buoy-theme"
import colors from "../src/components/buoy-theme/colors"

// We use border-box in all our projects so it's worth adding here.
const GlobalStyle = createGlobalStyle`
  *,
  *::before,
  *::after {
    box-sizing: border-box;
  }
`
const withThemeProvider = (Story, context) => {
  return (
    <Provider>
      <GlobalStyle />
      <Story {...context} />
    </Provider>
  )
}
export const decorators = [withThemeProvider]

export const parameters = {
  backgrounds: {
    default: "White",
    values: [
      { name: "White", value: colors.white },
      { name: "Blue", value: colors.primary },
      { name: "Grey", value: colors.neutralLightest },
    ],
  },
  knobs: { escapeHTML: false },
}
